import AboutUs from '../components/AboutUs.js'

function AboutUsPage(){

    return (
        <div align = "center">
            <br/>
            <h1>ผู้จัดทำเว็บนี้</h1>
            <AboutUs name = 
            "Kittithat Kanchanathaworn"
            />
            <br/>
        </div>
    ); 
}

export default AboutUsPage;