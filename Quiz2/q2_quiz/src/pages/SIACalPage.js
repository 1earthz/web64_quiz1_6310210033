import SIAResult from '../components/SIAResult.js'
import {useState} from "react";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box } from '@mui/material';
function SIACalPage() {

    const [ name, setName ] = useState("");
    const [ siaResult, setSiaResult ] = useState("");
    const [ TranslateResult, setTranslateResult ] = useState("");

    const [year, setYear] = useState("");
    const [birth, setBirth] = useState("");

    function calculateSIA(){
        let y = parseFloat(year);
        let be = parseFloat(birth);
        let sia = (be*y)/200000;
        setSiaResult(sia);
        if(sia>0.5){
            setTranslateResult("ปีนี้จะโชคดี");
        }else if(sia<0.5){
            setTranslateResult("ปีนี้จะโชคร้าย");
        }else{
            setTranslateResult("เฉยๆ");
        }
    }

    return(
        <Grid container spacing={2} sx= {{ marginTop : "10px"}}>
            <Grid item xs={12}>
                <Typography variant="h3"> 
                    Welcome to SIA Calculator
                </Typography>
            </Grid>
            <Grid 
            item xs={8}>
            <Box sx={{ textAlign : "canter"}} > <br /> <br /> <br /> <br /> <br /> <br />
                        ชื่อ : <input type = "text" 
                        value={name} onChange={(e) => { setName(e.target.value);}}/> <br />
                       
                        อายุ (y) : <input type = "text" 
                        value={year} onChange={(e) =>{setYear(e.target.value);}}/>  <br /> 
                       
                        ปีเกิด (be) : <input type = "text" 
                        value={birth} onChange={(e) => {setBirth(e.target.value);}}/>   <br /> <br /> <br />
            
                        <Button variant="contained" onClick={ () => {calculateSIA()}}>
                            Calculate
                        </Button>
            </Box>
            </Grid>
            <Grid item xs={4}>  <br /> <br /> <br /> 
            { siaResult != 0 && 
                <div>
                    <h3>ผลการคำนวณ</h3>
                    <SIAResult
                        name ={ name }
                        SIA = { siaResult }
                        result = {TranslateResult}
                    />
                </div>
            }
            </Grid>
        </Grid>
   /* <div align = "left">
        <div align = "center">
            <br/>
            <h1>Welcome to SIA Calculator</h1>
            <br/>

            <h2>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
            </h2>

            <br/>
            <h3>อายุ (y) : <input type = "text" value={year} onChange={(e) =>{setYear(e.target.value);}}/>
            </h3>

            <h3>ปีเกิด (be) : <input type = "text" value={birth} onChange={(e) => {setBirth(e.target.value);}}/></h3>
            <br />
             <Button variant="contained" onClick={ () => {calculateSIA() } }>
                 Calculate
            </Button>
            <br/>
            <br/>
            <br/>

            <Grid item xs={4}>
                { siaResult != 0 && 
                <div>
                    <h3>ผลการคำนวณ</h3>
                    <SIAResult
                        name ={ name }
                        SIA = { siaResult }
                        result = {TranslateResult}
                    />
                </div>
            }
            </Grid>     
        </div>
    </div>*/
    );
}

export default SIACalPage;