import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import SIACalPage from './pages/SIACalPage';
import Header from './components/Header';

import { Routes, Route } from "react-router-dom";


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element ={
              <AboutUsPage/>
          }/>

          <Route path="/" element ={
              <SIACalPage/>
          }/>
      </Routes>
    </div>
  );
}

export default App;
