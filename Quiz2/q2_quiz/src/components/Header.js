import {Link} from "react-router-dom"

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant="h6">
                Welcome to SIA calculator : &nbsp;
            </Typography> 
            
            &nbsp; &nbsp;
            <Link to="/">
                 <Typography variant="body1">
                พยากรณ์ดวง
            </Typography>

            </Link>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
            
            <Link to="about">
                <Typography variant="body1">
                ผู้จัดทำ
            </Typography>
                </Link>
                &nbsp; &nbsp; &nbsp;

        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;

/*function Header() {

    return (
        <div align = "left">
            <p>Welcome to SIA calculator : &nbsp;
                <Link to="/">พยากรณ์ดวง</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="about">ผู้พัฒนา</Link>
                &nbsp; &nbsp; &nbsp;
            </p>
        </div>
    );
}

export default Header;*/