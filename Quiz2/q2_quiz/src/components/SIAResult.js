function SIAResult (props){

    return (

        <div id = "BG">
            <h3>You : {props.name} </h3>
            <h3>SIA : {props.SIA} </h3>
            <h3>Result : {props.result} </h3>
        </div>
    );
}
export default SIAResult;